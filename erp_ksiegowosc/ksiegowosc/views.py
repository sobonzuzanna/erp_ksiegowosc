from django.shortcuts import render, redirect
from django.contrib.auth.decorators import login_required
from django.contrib.auth import authenticate, login

def login_view(request):
    if request.method == 'POST':
        username = request.POST.get('username')
        password = request.POST.get('password')
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            return redirect('dashboard')
        else:
            return render(request, 'logowanie.html', {'error': 'Nieprawidłowa nazwa użytkownika lub hasło'})
    else:
        return render(request, 'logowanie.html')
    
def dashboard_view(request):
    return render(request, 'dashboard.html')